all: con_def.c
	gcc con_def.c -o con_def -DTEST1 -DTEST2

test1: con_def.c
	gcc con_def.c -o con_def -DTEST1

test2: con_def.c
	gcc con_def.c -o con_def -DTEST2

nodef: con_def.c
	gcc con_def.c -o con_def

clean:
	rm con_def
