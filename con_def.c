#include <stdio.h>

int main()
{
#if defined(TEST1)
    printf("TEST1 defined\n");
#elif defined(TEST2)
    printf("No TEST1, but TEST 2 defined\n");
#else
    printf("TEST1 and TEST2 are not defined\n");
#endif

    return 0;
}
